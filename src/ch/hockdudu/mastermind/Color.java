package ch.hockdudu.mastermind;

import java.security.InvalidParameterException;
import java.util.Random;

public enum Color {

    RED('r'),
    GREEN('g'),
    BLUE('b'),
    YELLOW('y'),
    WHITE('w'),
    BLACK('k');


    public char inputChar;

    Color(char inputChar) {
        this.inputChar = inputChar;
    }

    private static Random random = new Random();

    public static Color getRandomColor() {
        var colorsLength = Color.values().length;
        var randomIndex = random.nextInt(colorsLength);
        return Color.values()[randomIndex];
    }

    public static Boolean isColorValid(char userChar) {
        var colors = Color.values();
        for (var color : colors) {
            if (color.inputChar == userChar)
                return true;
        }
        return false;
    }

    public String toString() {
        return String.valueOf(inputChar);
    }

    public static Color createColorFromChar(char userChar) {
        var colors = Color.values();
        for (var color : colors) {
            if (color.inputChar == userChar)
                return color;
        }
        throw new InvalidParameterException();
    }
}
