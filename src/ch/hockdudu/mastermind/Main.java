package ch.hockdudu.mastermind;

import java.util.Arrays;
import java.util.Scanner;

public class Main {

    private static int colorNumber = 4;
    private static int gameDuration = 12;

    public static void main(String[] args) {
        var secretCode = new Color[colorNumber];
        Arrays.setAll(secretCode, i -> Color.getRandomColor());

        System.out.println(Arrays.toString(Color.values()));

        var didPlayerWonGame = false;
        for (var i = 0; i < gameDuration; i++) {
            var userColors = getUserColors();
            var pegs = VerificationPeg.compareColors(secretCode, userColors);

            var correctColorAndPlace = Arrays.stream(pegs).filter(t -> t == VerificationPeg.CORRECT_COLOR_AND_PLACE).count();
            var correctColor = Arrays.stream(pegs).filter(t -> t == VerificationPeg.CORRECT_COLOR).count();

            System.out.println(correctColorAndPlace + " with correct color and place, " + correctColor + " with correct color");

            if (Arrays.stream(pegs).allMatch(j -> j == VerificationPeg.CORRECT_COLOR_AND_PLACE)) {
                didPlayerWonGame = true;
                break;
            }
        }

        if (didPlayerWonGame) {
            System.out.println("You won");
        } else {
            System.out.println("You lost, code was " + Arrays.toString(secretCode));
        }
    }

    private static Color[] getUserColors() {
        var isUserInputCorrect = false;

        var scanner = new Scanner(System.in);
        var userInput = "";
        var outColors = new Color[colorNumber];

        while (!isUserInputCorrect) {
            userInput = scanner.nextLine();
            if (userInput.length() != colorNumber) {
                continue;
            }

            isUserInputCorrect = userInput.chars().allMatch(i -> Color.isColorValid((char) i));

            if (isUserInputCorrect) {
                var userInputChars = userInput.toCharArray();
                for (int i = 0; i < userInputChars.length; i++) {
                    outColors[i] = Color.createColorFromChar(userInputChars[i]);
                }
            }
        }

        return outColors;
    }
}
