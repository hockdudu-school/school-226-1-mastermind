package ch.hockdudu.mastermind;

import java.security.InvalidParameterException;
import java.util.Arrays;

public enum VerificationPeg {

    CORRECT_COLOR("c"),
    CORRECT_COLOR_AND_PLACE("cp"),
    INCORRECT("i");


    public String typedName;

    VerificationPeg(String name) {
        this.typedName = name;
    }


    public static VerificationPeg[] compareColors(Color[] compareColors, Color[] userColors) {
        if (compareColors.length != userColors.length) {
            throw new InvalidParameterException("Parameters' length aren't equal");
        }

        var outPeg = new VerificationPeg[compareColors.length];

        for (var i = 0; i < userColors.length; i++) {
            if (userColors[i] == compareColors[i]) {
                outPeg[i] = VerificationPeg.CORRECT_COLOR_AND_PLACE;
                continue;
            }

            int finalI = i;
            var anyColorMatch = Arrays.stream(compareColors).anyMatch(j -> j == userColors[finalI]);

            if (anyColorMatch) {
                outPeg[i] = VerificationPeg.CORRECT_COLOR;
            } else {
                outPeg[i] = VerificationPeg.INCORRECT;
            }
        }

        return outPeg;
    }


    public String toString() {
        return this.typedName;
    }
}